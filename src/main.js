(function() {
  const canvas = document.querySelector('.boot');
  const rect = canvas.getBoundingClientRect();
  const balls = [];

  let animationSource,
      removedCounter = 0,
      currentCounter = 0,
      totalCounter = 0,
      WIDTH = rect.width,
      HEIGHT = rect.height,
      INFELICITY = 50,
      isMuted = true,
      isPaused = false;

  canvas.width = WIDTH;
  canvas.height = HEIGHT;

  const statusBar = document.createElement('div');
  statusBar.classList.add('statusBar');

  const currentBallsCounter = document.createElement('span');
  currentBallsCounter.classList.add('counter');
  currentBallsCounter.innerHTML = `Current Balls: ${currentCounter}`;

  const removedBallsCounter = document.createElement('span');
  removedBallsCounter.classList.add('counter');
  removedBallsCounter.innerHTML = `Removed Balls: ${removedCounter}`;

  const totalBallsCounter = document.createElement('span');
  totalBallsCounter.classList.add('counter');
  totalBallsCounter.innerHTML = `Total Balls: ${totalCounter}`;

  const muteIndicator = document.createElement('span');
  muteIndicator.classList.add('counter', 'counter--control');
  muteIndicator.innerHTML = '🔇';

  const pauseIndicator = document.createElement('span');
  pauseIndicator.classList.add('counter', 'counter--control');
  pauseIndicator.innerHTML = '⏸️';

  const patreon = document.createElement('button');
  patreon.classList.add('patreon');
  patreon.onclick = () => {
    ym(54258025,'reachGoal','patreon');
    window.open('https://www.patreon.com/bePatron?u=29982116');
  };

  const patreonImage = document.createElement('img');
  patreonImage.setAttribute('src', 'https://c5.patreon.com/external/logo/become_a_patron_button.png');
  patreonImage.setAttribute('title', 'Become a Patron!');
  patreonImage.classList.add('patreon__image');

  patreon.appendChild(patreonImage);

  const legend = document.createElement('span');
  legend.classList.add('legend');
  legend.innerHTML = 'Press Space for Pause. Press M for sound.';

  statusBar.appendChild(legend);
  statusBar.appendChild(currentBallsCounter);
  statusBar.appendChild(removedBallsCounter);
  statusBar.appendChild(totalBallsCounter);
  statusBar.appendChild(muteIndicator);

  document.body.appendChild(patreon);
  document.body.appendChild(statusBar);

  canvas.addEventListener('mousemove', (e) => {
    removeBall(e);
  });

  canvas.addEventListener('touchmove', (e) => removeBall(e));

  const removeBall = (e) => {
    const { clientX, clientY } = e;
    const ball = balls.find(_ball => {
      const x = Math.floor(_ball.x);
      const y = Math.floor(_ball.y);

      if (
        (clientX - x) <= INFELICITY && 
        (clientX - x) > 0 &&
        (clientY - y) <= INFELICITY &&
        (clientY - y) > 0
      ) {
        return true;
      }
      if (
        (x - clientX) <= 25 && 
        (x - clientX) > 0 && 
        (y - clientY) <= 25 &&
        (y - clientY) > 0
      ) {
        return true;
      }
    });

    if (ball && !isPaused) {
      ball.isDestroyed = true;
      ball.red = 255;
      ball.green = 255;
      ball.blue = 255;
  
      if (!isMuted) {
        const fart = Math.floor(Math.random() * 7);
        const puff = new Audio(`./puff/${fart === 0 ? 1 : fart}.mp3`);
        puff.volume = .05;
        puff.play();
      }
  
      setTimeout(() => {
        balls.splice(balls.indexOf(ball), 1);
      }, 100);
  
      currentCounter--;
      currentBallsCounter.innerHTML = `Current Balls: ${currentCounter}`;
  
      removedCounter++;
      removedBallsCounter.innerHTML = `Removed Balls: ${removedCounter}`;

      if (removedCounter === 10) ym(54258025,'reachGoal','destroy_10');
      if (removedCounter === 100) ym(54258025,'reachGoal','destroy_100');
      if (removedCounter === 1000) ym(54258025,'reachGoal','destroy_1000');
      if (removedCounter === 10000) ym(54258025,'reachGoal','destroy_10000');
    } 
  }

  const createBall = () => {
    const ball = {};
    ball.isDestroyed = false;

    ball.x = Math.floor(Math.random() * WIDTH);
    ball.y = Math.floor(Math.random() * HEIGHT);
  
    ball.hSpeed = Math.random() * 2 - 1;
    ball.vSpeed = Math.random() * 2 - 1;

    ball.d = Math.floor(Math.random() * 10);

    ball.red = Math.floor(Math.random(0) * 255);
    ball.green = Math.floor(Math.random(0) * 255);
    ball.blue = Math.floor(Math.random() * 255);

    totalCounter++;
    totalBallsCounter.innerHTML = `Total Balls: ${totalCounter}`;

    currentCounter++;
    currentBallsCounter.innerHTML = `Current Balls: ${currentCounter}`;
  
    return ball;
  }

  const draw = () => {
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, WIDTH, HEIGHT);


    balls.map(ball => {
      ball.x += ball.hSpeed;
      ball.y += ball.vSpeed;

      if (ball.isDestroyed) ball.d++;

      if (ball.x < 0 || ball.x > WIDTH) ball.hSpeed = -ball.hSpeed;
      if (ball.y < 0 || ball.y > HEIGHT) ball.vSpeed = -ball.vSpeed;

      ctx.beginPath();

      ctx.arc(ball.x, ball.y, ball.d, 0 , 2 * Math.PI);
      ctx.fillStyle = `rgb(${ball.red}, ${ball.green}, ${ball.blue})`;

      ctx.fill();
    });

    animationSource = requestAnimationFrame(draw);
  }

  const pause = () => {
    isPaused = !isPaused;

    if (!isPaused) {
      statusBar.removeChild(pauseIndicator);
      draw();
    }
    else {
      statusBar.appendChild(pauseIndicator);
      cancelAnimationFrame(animationSource);

      ym(54258025,'reachGoal','pause');
    }
  }

  const mute = () => {
    isMuted = !isMuted;

    if (isMuted) {
      statusBar.appendChild(muteIndicator);
    } else {
      statusBar.removeChild(muteIndicator);
      ym(54258025,'reachGoal','sound_on');
    }
  }

  draw();


  window.addEventListener('keypress', (e) => {
    const { keyCode } = e;

    if (keyCode === 32) pause();
    if (keyCode === 109) mute();
  });
  
  window.addEventListener('resize', (e) => {
    const rect = canvas.getBoundingClientRect();

    WIDTH = rect.width;
    HEIGHT = rect.height;
  
    canvas.width = WIDTH;
    canvas.height = HEIGHT;
  });

  setInterval(() => {
    if (!isPaused) balls.push(createBall());
  }, 50);
})();