FROM nginx:alpine

RUN rm -rf /etc/nginx/nginx.conf

COPY ./nginx.conf /etc/nginx/nginx.conf

# Add user so we don't need --no-sandbox.
RUN addgroup -S developer && adduser -S -g developer developer && mkdir -p /app

# Run everything after as non-privileged user.
WORKDIR /app
COPY ./src .

RUN chown -cR developer:developer /app

EXPOSE 80